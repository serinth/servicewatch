# README #

This service watch application monitors CPU usage, memory and page file on a set of machines defined by their machine name on a network. 
It also uses impersonation to start, stop, pause, resume and restart services that it's currently monitoring.

Screenshots and a further explanation can be found at [tonytruong.net](http://www.tonytruong.net)

Drop me a line if you have questions truong.tony@live.com