﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;
using System.Reflection;

namespace ServiceWatch.Services
{
    public class FileLog:ILogger
    {
        private readonly NLog.Logger _logger;

        public FileLog()
        {
            _logger = LogManager.GetLogger(Assembly.GetCallingAssembly().FullName);
        }

        public void Log(string message)
        {
            _logger.Log(LogLevel.Info, message);
        }
    }
}