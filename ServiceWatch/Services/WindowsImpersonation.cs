﻿using SimpleImpersonation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceWatch.Services
{
    public class WindowsImpersonation
    {
        private readonly static string _username = System.Configuration.ConfigurationManager.AppSettings["ImpersonationUser"];
        private readonly static string _password = System.Configuration.ConfigurationManager.AppSettings["ImpersonationPassword"];
        private readonly static string _domain = System.Configuration.ConfigurationManager.AppSettings["ImpersonationDomain"];

        protected WindowsImpersonation() { }

        public static T ExecuteFunction<T>(Func<T> function)
        {
            T result;
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                using (Impersonation.LogonUser(_domain, _username, _password, LogonType.Interactive))
                {
                    result = function();
                }
            }
            else
            {
                result = function();
            }

            return result;
        }
    }
}