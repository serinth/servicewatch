﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ServiceWatch.Models
{
    public class MachinePerformance
    {
        public string MachineName { get; private set; }
        private PerformanceCounter _cpuCounter;
        private PerformanceCounter _ramCounter;
        private PerformanceCounter _pageCounter;

        public string CPU
        {
            get
            {
                _cpuCounter.NextValue();
                System.Threading.Thread.Sleep(1000);
                return String.Format("{0:##0} %", _cpuCounter.NextValue());
            }
        }
        public string RAMRemaining { get { return String.Format("{0} MB", _ramCounter.NextValue()); } }
        public string PageFileUsage { get { return String.Format("{0:##0} %", _pageCounter.NextValue()); } }

        public MachinePerformance(string machineName)
        {
            this.MachineName = machineName;

            try
            {
                _cpuCounter = new PerformanceCounter
                    ("Processor", "% Processor Time", "_Total", MachineName);

                _ramCounter = new PerformanceCounter
                    ("Memory", "Available MBytes", String.Empty, MachineName);

                _pageCounter = new PerformanceCounter
                    ("Paging File", "% Usage", "_Total", MachineName);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Could not find machine name: {0}", MachineName));
            }
        }
    }
}