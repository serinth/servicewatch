﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceWatch.Models.Requests
{
    public class ServiceItemRequest
    {
        public string ServiceName { get; set; }
        public string MachineName { get; set; }
    }
}