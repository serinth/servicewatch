﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Web;

namespace ServiceWatch.Models
{
    public class WindowsService
    {
        private ServiceController _service;
        public string MachineName { get; private set; }
        public string Name { get { return _service.DisplayName; } }
        public string Status { get { return _service.Status.ToString(); } }
        
        public WindowsService(string serviceName, string machineName)
        {
            this.MachineName = machineName;
            _service = new ServiceController(serviceName, machineName);
        }

        public void Restart()
        {
            if (_service.CanStop)
            {
                _service.Stop();
                _service.Start();
            }
        }

        public void Pause()
        {
            if (_service.CanPauseAndContinue)
            {
                _service.Pause();
            }
        }

        public void Resume()
        {
            _service.Continue();
        }

        public void Stop()
        {
            if (_service.CanStop)
            {
                _service.Stop();
            }
        }

        public void Start()
        {
            _service.Start();
        }
    }
}