﻿using ServiceWatch.Models;
using ServiceWatch.Models.Requests;
using ServiceWatch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceWatch.Controllers
{
    public class ServiceController : ApiController
    {
        private readonly ILogger _logger;

        public ServiceController(ILogger logger)
        {
            this._logger = logger;
        }

        [HttpGet]
        public WindowsService Status(string serviceName, string machineName)
        {
            WindowsService service = null;
            WindowsImpersonation.ExecuteFunction<bool>(() =>
                {
                    try
                    {
                        service = new WindowsService(serviceName, machineName);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Log(ex.ToString());
                        return false;
                    }
                });
            
            return service;
        }

        [HttpPost]
        public void Start(ServiceItemRequest request)
        {
            WindowsImpersonation.ExecuteFunction<bool>(() =>
                {
                    try
                    {
                        var service = new WindowsService(request.ServiceName, request.MachineName);
                        service.Start();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        _logger.Log(ex.ToString());
                        return false;
                    }
                });
        }

        [HttpPost]
        public void Stop(ServiceItemRequest request)
        {
            WindowsImpersonation.ExecuteFunction<bool>(() =>
            {
                try
                {
                    var service = new WindowsService(request.ServiceName, request.MachineName);
                    service.Stop();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.ToString());
                    return false;
                }
            });
        }

        [HttpPost]
        public void Restart(ServiceItemRequest request)
        {
            WindowsImpersonation.ExecuteFunction<bool>(() =>
            {
                try
                {
                    var service = new WindowsService(request.ServiceName, request.MachineName);
                    service.Restart();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.ToString());
                    return false;
                }
            });
        }

        [HttpPost]
        public void Pause(ServiceItemRequest request)
        {
            WindowsImpersonation.ExecuteFunction<bool>(() =>
            {
                try
                {
                    var service = new WindowsService(request.ServiceName, request.MachineName);
                    service.Pause();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.ToString());
                    return false;
                }
            });
        }

        [HttpPost]
        public void Resume(ServiceItemRequest request)
        {
            WindowsImpersonation.ExecuteFunction<bool>(() =>
            {
                try
                {
                    var service = new WindowsService(request.ServiceName, request.MachineName);
                    service.Resume();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.ToString());
                    return false;
                }
            });
        }
    }
}
