﻿using ServiceWatch.Models;
using ServiceWatch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceWatch.Controllers
{
    public class PerformanceController : ApiController
    {
        
        private readonly ILogger _logger;

        public PerformanceController(ILogger logger)
        {
            this._logger = logger;
        }
        
        public object Get(string machineName)
        {
            MachinePerformance machine = null;
            WindowsImpersonation.ExecuteFunction<bool>(() =>
            {
                try
                {
                    machine = new MachinePerformance(machineName);
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.ToString());
                    return false;
                }
            });
            
            return machine;
        }
    }
}
