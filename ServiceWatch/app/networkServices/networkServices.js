﻿(function () {
    'use strict';
    var controllerId = 'networkServices';
    angular.module('app').controller(controllerId, ['$route', 'common', 'datacontext', networkServices]);

    function networkServices($route, common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.services = null;
        vm.serviceStatusCollection = [];
        vm.startService = startService;
        vm.stopService = stopService;
        vm.pauseService = pauseService;
        vm.resumeService = resumeService;
        vm.restartService = restartService;
        activate();


        function activate() {
            var promises = [getServices()];
            common.activateController(promises, controllerId)
                .then(function () { log('Activated Network Services Overview'); });
        }

        function getServices() {
            return datacontext.getServices().then(function (data) {
                vm.services = data;
                vm.services.forEach(function (service) {
                    datacontext.getServiceStatus(service).then(function (status) {
                        vm.serviceStatusCollection.push(status);
                    });
                });
                return vm.services;
            });
        }

        function startService(service) {
            var request = { ServiceName: service.Name, MachineName: service.MachineName };
            return datacontext.startService(request).then(function (data) {
                $route.reload();
            });            
        }
        function stopService(service) {
            var request = { ServiceName: service.Name, MachineName: service.MachineName };
            return datacontext.stopService(request).then(function (data) {
                $route.reload();
            });
        }
        function restartService(service) {
            var request = { ServiceName: service.Name, MachineName: service.MachineName };
            return datacontext.restartService(request).then(function (data) {
                $route.reload();
            });
        }
        function pauseService(service) {
            var request = { ServiceName: service.Name, MachineName: service.MachineName };
            return datacontext.pauseService(request).then(function (data) {
                $route.reload();
            });
        }
        function resumeService(service) {
            var request = { ServiceName: service.Name, MachineName: service.MachineName };
            return datacontext.resumeService(request).then(function (data) {
                $route.reload();
            });
        }
        

    }
})();