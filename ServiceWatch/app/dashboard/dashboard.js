﻿(function () {
    'use strict';
    var controllerId = 'dashboard';
    angular.module('app').controller(controllerId, ['common', 'datacontext', dashboard]);

    function dashboard(common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.networkedMachines = null;
        vm.machines = [];
        vm.refreshMachines = refreshMachineInfo;
        activate();


        function activate() {
            var promises = [getNetworkedMachines()];
            common.activateController(promises, controllerId)
                .then(function () { log('Activated Network Machine Overview'); });
        }
        
        function getNetworkedMachines() {
            return datacontext.getNetworkMachines().then(function (data) {
                vm.networkedMachines = data;
                vm.networkedMachines.forEach(function (machine) {
                    datacontext.getMachinePerformance(machine.MachineName).then(function (machinePerf) {
                        vm.machines.push(machinePerf);
                    });
                });
                return vm.networkedMachines;
            });
        }

        function refreshMachineInfo() {
            setInterval(function () {
                vm.machines.forEach(function (machine, index) {
                    datacontext.getMachinePerformance(machine.MachineName).then(function (machinePerf) {
                        vm.machines[index] = machinePerf;
                    });
                });
            }, 1000);
        }

    }
})();