(function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId, ['$http','common', datacontext]);

    function datacontext($http,common) {
        var $q = common.$q;

        var service = {
            getNetworkMachines: getNetworkMachines,
            getServices: getServices,
            getMachinePerformance: getMachinePerformance,
            getServiceStatus: getServiceStatus,
            startService: startService,
            stopService: stopService,
            pauseService: pauseService,
            resumeService: resumeService,
            restartService:restartService
        };

        return service;



        function getNetworkMachines() {
            var networkedMachines = [
                { MachineName: "AUNBDEV01" },
                { MachineName: "AUNBDEV01" },
                { MachineName: "AUNBDEV01" },
                { MachineName: "AUNBDEV01" },
                { MachineName: "AUNBDEV01" }
            ];

            return $q.when(networkedMachines);
        }

        function getServices() {
            var services = [
                { ServiceName: encodeURIComponent("COM+ Event System"), MachineName: "AUNBDEV01" },
                { ServiceName: encodeURIComponent("Credential Manager"), MachineName: "AUNBDEV01" },
                { ServiceName: encodeURIComponent("SQL Server (SQLEXPRESS)"), MachineName: "AUNBDEV01" }
            ];

            return $q.when(services);
        }

        function getMachinePerformance(machineName) {
            var deferred = $q.defer();
            $http.get(common.webapiLocation + "Performance/Get?machineName=" + machineName).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }

        function getServiceStatus(serviceRequest) {
            var deferred = $q.defer();
            $http.get(common.webapiLocation + "Service/Status?serviceName=" + serviceRequest.ServiceName + "&machineName=" + serviceRequest.MachineName).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }

        function startService(serviceRequest) {
            var deferred = $q.defer();
            var request = { ServiceName: serviceRequest.ServiceName, MachineName: serviceRequest.MachineName };
            $http.post(common.webapiLocation + "Service/Start", request).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }

        function stopService(serviceRequest) {
            var deferred = $q.defer();
            var request = { ServiceName: serviceRequest.ServiceName, MachineName: serviceRequest.MachineName };
            $http.post(common.webapiLocation + "Service/Stop", request).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }

        function restartService(serviceRequest) {
            var deferred = $q.defer();
            var request = { ServiceName: serviceRequest.ServiceName, MachineName: serviceRequest.MachineName };
            $http.post(common.webapiLocation + "Service/Restart", request).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }

        function pauseService(serviceRequest) {
            var deferred = $q.defer();
            var request = { ServiceName: serviceRequest.ServiceName, MachineName: serviceRequest.MachineName };
            $http.post(common.webapiLocation + "Service/Pause", request).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }

        function resumeService(serviceRequest) {
            var deferred = $q.defer();
            var request = { ServiceName: serviceRequest.ServiceName, MachineName: serviceRequest.MachineName };
            $http.post(common.webapiLocation + "Service/Resume", request).success(function (data) { deferred.resolve(data) });
            return deferred.promise;
        }



    }
})();